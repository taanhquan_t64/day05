<!DOCTYPE html>
<html lang="vn">
<head>
    <meta charset="UTF-8">
</head>
    <title>Sign Up</title>
<body>
    
    <fieldset style='width: 500px; height: 450px; margin: auto; margin-top: 20px; boder:#1E90FF solid'>
    <?php 
        if (isset($_POST['submit'])) {
            session_start();
            $nameErr = $genderErr = $majorErr = $dateErr = "";
            $name = $gender = $major = $date = $address = $image = "";
            $_SESSION['name'] = $name;
            $_SESSION['gender'] = $gender;
            $_SESSION['major'] = $major;
            $_SESSION['date'] = $date;
            $_SESSION['address'] = $address;
            $_SESSION['image'] = $image;

            $dir = "php/day05/upload/";
            $check = file_exists($dir);
            if ($check == FALSE){
                mkdir("php/day05/upload/", 0700);
            }

            $filename = basename($_FILES["image"]["name"]);
            date_default_timezone_set("Asia/Ho_Chi_Minh");
            $newName = $filename .date("Y-m-d h:i:sa");
            rename($filename, $newName);
            $folder = $dir.$filename;
            move_uploaded_file($_FILES['image']['tmp_name'], $folder);
            

            if (empty($_POST['name']))
                $err['name'] = "<label class='error'>Hãy nhập tên.</label><br>";
            else
                $_SESSION["name"] = $_POST["name"];
                
            if (empty($_POST['gender']))
                $err['gender'] = "<label class='error'>Hãy chọn giới tính.</label><br>";
            else
                $_SESSION["gender"] =  $_POST["gender"];
        
            if (empty($_POST['department']))
                $err['department'] = "<label class='error'>Hãy chọn phân khoa. <br /></label>";
            else
                $_SESSION["department"] = $_POST["department"];

            if (empty($_POST['address']))
                $err['address'] = "<label class='error'>Hãy nhập ảnh.</label><br>";
            else
                $_SESSION["address"] =  $_POST["address"];
            
            if (empty($_POST['image']))
                $err['image'] = "<label class='error'>Hãy chọn ảnh.</label><br>";
            else
                $_SESSION["image"] =  $_POST["iamge"];    
        
            $date = $_POST['dob'];
        
            function isValid($date, $format = 'd/m/Y'){
                $dt = DateTime::createFromFormat($format, $date);
                return $dt && $dt->format($format) === $date;
            }
        
            if (empty($_POST['dob']))
                $err['dob'] = "<label class='error'>Hãy chọn ngày sinh. <br /></label>";
            else
                if (isValid($date) == false)
                    $err['dob'] = "<label class='error'>Hãy chọn đúng định dạng ngày sinh (d/m/Y).</label><br>";        
                else
                    $_SESSION["dob"] = $_POST["dob"];
            
            $_SESSION["address"] = $_POST["address"];
        
            $_SESSION["image"] = $folder;
        
            if ($_SESSION["name"] != "" && $_SESSION["gender"] != "" && $_SESSION["department"] != "" & $_SESSION["dob"] != "")
                header('Location: day05_submit.php');
        }
        
        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }
        function validateDate($date){
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
                return true;
            } else {
                return false;
            }
        }

        
    ?>
    <form action='day05_submit.php' style='margin: 20px 50px 0 30px' method="post">
        <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
                <tr height = '40px'>
                    <td width = 30% style = 'background-color: #32CD32; 
                    vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label style='color: white;'>Họ và tên<span style='color:red;'>*</span></label>
                    </td>
                    <td width = 30% >
                        <input type='string' name = 'name' style = 'line-height: 34px; border-color:#32CD32'>
                        <?php echo isset($_POST['name']) ? $_POST['name'] : ''; ?>
                    </td>
                </tr>
                <tr height = '40px'>
                    <td width = 30% style = 'background-color: #32CD32; vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label style='color: white;'>Giới tính<span style='color:red;'>*</span></label>
                    </td>
                    <td width = 30% >
                    <?php
                        $gender=array("Nam","Nữ");
                        for ($i = 0; $i < count($gender); $i++) {
                            echo
                                "<input type='radio' name='gender' class='gender' value='" . $i . "'";
                                echo (isset($_POST['gender']) && $_POST['gender'] == $gender[$i]) ? " checked " : "";
                            echo "/>" . $gender[$i];
                        }
                    ?>  

                    </td>
                </tr>


                <tr height = '40px'>
                    <td style = 'background-color: #32CD32; vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label style='color: white;'>Phân Khoa<span style='color:red;'>*</span></label>
                    </td>
                    <td height = '40px'>
                        <select name='department' style = 'border-color:#82cd79;height: 100%;width: 80%;'>
                            <option value="0" > Toán cơ tin học </option>
                            <option value="1" > Sinh học </option>
                            <option value="2" > Vật lý </option>
                            <!-- <option value="3" > Địa chất </option> -->
                            <!-- <option value="4" > Khác </option> -->
                        </select>
                    </td>
                </tr>

                <tr height = '40px'>
                    <td style = 'background-color: #32CD32; vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label style='color: white;'>Ngày sinh<span style='color:red;'>*</span></label>
                    </td>
                    <td height = '40px'>
                        <input type='date' name='dob' type='text' data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border-color:#82cd79'>
                    </td>
                </tr>
                <tr height = '40px'>
                    <td style = 'background-color: #32CD32; vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label style='color: white;'>Địa chỉ</label>
                    </td>
                    <td height = '40px'>
                        <input type='string' name='address' style = 'line-height: 32px; border-color:#32CD32'>
                    </td>
                </tr>

                <tr height = '40px'>
                    <td style = 'background-color: #32CD32; vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label for='hinhanh' style='color: white;'>Hình ảnh</label>
                    </td>
                    <td height = '40px'>
                    <form action="day05_submit.php" method="post" enctype="multipart/form-data">
                        <input type="file" name="img">
                    
                    </form>
                    </td>
                </tr>
            </table>
            <button style='background-color: #32CD32; border-radius: 5px; 
            width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Đăng Kí</button>
        </form>
    </fieldset>
</body>
</html>